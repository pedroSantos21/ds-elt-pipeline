'''
Example how to build a short ELT (Extract, Load and Tranform)
pipeline using python and dummyJSON (https://dummyjson.com/), a fake REST API.


GOAL: Find out which customers spent the most 
money on the shop

Ths steps to follow are:
1. Extract the data from dummyJSON API
2. Load data (raw) to BigQuery 
    - BigQuery is Google's fully managed, serverless data warehouse that enables scalable analysis 
      over petabytes of data. It is a Platform as a Service (PaaS) that supports querying using ANSI SQL.
      It also has built-in machine learning capabilities. 
3. Execute the analytics
'''


import requests
import json
import os
from os.path import dirname, abspath
from google.cloud import bigquery

# BigQuery Service Account Key ()
os.environ['GOOGLE_APPLICATION_CREDETIALS'] = 'CloudDemo_Pedro_Service.json'


# EXTRACT DATA
ENDPOINT = "https://dummyjson.com/"

def call_api(resource:str):
    '''
    by default you will get 30 results and the total count, you can pass "skip" & "limit" query string to get more results.
    For example: /posts?skip=5&limit=10
    '''
    # Initializing total picked
    results_picked = 0
    total = 1000 # random
    full_data = []
    

    while results_picked < total:
        request = ENDPOINT+resource

        params = {"skip": results_picked}
        # REQUEST API AND GET THE RESPONSE
        response = requests.get(request, params=params)

        # Checkin status code
        if response.status_code == 200: #OK
            data = response.json()
            rows  = data.get(resource)
            full_data += rows
            total = data.get("total")
            results_picked += len(rows)

        else:
            raise Exception(response.text)
    
    return full_data

def download_json(data, name):
    file_path = f"{dirname(abspath(__file__))}/{name}.json"
    with open(file_path, "w") as file:
        file.write("\n".join([json.dumps(row) for row in data]))


def loadFile(resource, client):
    # TODO(developer): Set table_id to the ID of the table to create.
    # table_id = "your-project.your_dataset.your_table_name"
    table_id = f"phonic-axle-380212.elt_project.dummy_{resource}"


    job_config = bigquery.LoadJobConfig(
        source_format=bigquery.SourceFormat.NEWLINE_DELIMITED_JSON, 
        autodetect=True,
        write_disposition="write_truncate"
    )

    with open(f"{dirname(abspath(__file__))}/{resource}.json", "rb") as source_file:
        job = client.load_table_from_file(source_file, table_id, job_config=job_config)
        
        # wAITS FOR THE JOB TO COMPLETE
        job.result()

        # MAKE AN API REQUEST
        table = client.get_table(table_id)

        print(f"Loaded {table.num_rows} and {len(table.schema)} columns to {table_id}")

# Requestig Data to the API and storing it locally
users_data = call_api('users')
download_json(users_data, "users")
carts_data = call_api('carts')
download_json(carts_data, "carts")

# Creating a BigQuery Client Instance
client = bigquery.Client()

# # Loading data to bigquery
loadFile('carts', client)
loadFile('users', client)

# ADD TOP SPENDERS
query = """
SELECT
  U.id as USER_ID,
  U.firstName as USER_FIRST_NAME,
  U.lastName as USER_LAST_NAME,
  SUM(total) AS TOTAL_SPENT
FROM 
  phonic-axle-380212.elt_project.dummy_users AS U
LEFT JOIN
  phonic-axle-380212.elt_project.dummy_carts AS C
ON 
  U.id = C.id
GROUP BY
  u.id, USER_FIRST_NAME, USER_LAST_NAME
"""

query_config = bigquery.QueryJobConfig(
    destination="phonic-axle-380212.elt_project.dummy_best_spenders",
    write_disposition="write_truncate"
)

client.query(query, job_config=query_config)


